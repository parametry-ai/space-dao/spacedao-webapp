# Space DAO official frontend


Space DAO is best and official access is available at: https://app.spacedao.ai

Space DAO is a decentralized autonomous organization of orbit services and
further.
The project has been designed by [Parametry.ai](https://parametry.ai#gitlabSDW),
University of Strathclyde and Vision Space Technologies GmbH, during a
innovation initation (OSIP) supported by the European Space Agency.

Tech stack:
- Ionic Framework and UI components
- Vue.js
- [OpenAPI Typescript codegen](https://github.com/ferdikoomen/openapi-typescript-codegen)


## Install

Install all dependencies:
```
npm install
```

Update or generate TS/Axios code from an OpenAPI Json
```
./node_modules/openapi-typescript-codegen/bin/index.js -i src/spacedao/openapi_v01.json -o src/spacedao --client axios
```

## Run a local server


```
ionic serve --host=127.0.0.1 --no-open
```


## For developers

### Frontend

#### Installations

Everything is already set in the tracked package.json, but here are installations
that had to be made.

To install the state management pattern + library [vuex4](https://vuex.vuejs.org/installation.html) for Vue3
```
npm install --safe vuex@next
npm install axios vue-axios

```

To install the OpenAPI Typescript generator to generate callable functions in typescript to call Space DAO official API:
```
npm install openapi-typescript-codegen --save-dev
```

### Backend

You need to run API locally: [see instructions in Space DAO API](https://gitlab.com/parametry-ai/space-dao/spacedao-api)

