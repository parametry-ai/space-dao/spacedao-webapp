/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Account } from '../models/Account';
import type { AccountCreate } from '../models/AccountCreate';
import type { CDM } from '../models/CDM';
import type { CDMCreate } from '../models/CDMCreate';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class StmService {

    /**
     * Version
     * @returns any Successful Response
     * @throws ApiError
     */
    public static versionAppStmV1Get(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/',
            errors: {
                404: `Lost in STM 404 space`,
            },
        });
    }

    /**
     * Get Accounts
     * @returns Account Successful Response
     * @throws ApiError
     */
    public static getAccountsAppStmV1AccountsGet(): CancelablePromise<Array<Account>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/accounts',
            errors: {
                404: `Lost in STM 404 space`,
            },
        });
    }

    /**
     * Create Account
     * @param requestBody
     * @returns Account Successful Response
     * @throws ApiError
     */
    public static createAccountAppStmV1AccountsPost(
        requestBody: AccountCreate,
    ): CancelablePromise<Account> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/app/stm/v1/accounts',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

    /**
     * Get Account Id
     * @param accountId
     * @returns Account Successful Response
     * @throws ApiError
     */
    public static getAccountIdAppStmV1AccountsAccountIdGet(
        accountId: string,
    ): CancelablePromise<Account> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/accounts/{account_id}',
            path: {
                'account_id': accountId,
            },
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

    /**
     * Stats Rso
     * @returns any Successful Response
     * @throws ApiError
     */
    public static statsRsoAppStmV1StatsRsoGet(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/stats/rso',
            errors: {
                404: `Lost in STM 404 space`,
            },
        });
    }

    /**
     * Stats Rso Id
     * @param rsoId
     * @returns any Successful Response
     * @throws ApiError
     */
    public static statsRsoIdAppStmV1StatsRsoRsoIdGet(
        rsoId: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/stats/rso/{rso_id}',
            path: {
                'rso_id': rsoId,
            },
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

    /**
     * Stats Cdm
     * @returns any Successful Response
     * @throws ApiError
     */
    public static statsCdmAppStmV1StatsCdmGet(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/stats/cdm',
            errors: {
                404: `Lost in STM 404 space`,
            },
        });
    }

    /**
     * Stats Cdm Id
     * @param cdmId
     * @returns any Successful Response
     * @throws ApiError
     */
    public static statsCdmIdAppStmV1StatsCdmCdmIdGet(
        cdmId: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/stats/cdm/{cdm_id}',
            path: {
                'cdm_id': cdmId,
            },
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

    /**
     * Stats Suppliers
     * @returns any Successful Response
     * @throws ApiError
     */
    public static statsSuppliersAppStmV1StatsSuppliersGet(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/stats/suppliers',
            errors: {
                404: `Lost in STM 404 space`,
            },
        });
    }

    /**
     * Stats Supplier Id
     * @param supplierId
     * @returns any Successful Response
     * @throws ApiError
     */
    public static statsSupplierIdAppStmV1StatsSupplierSupplierIdGet(
        supplierId: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/stats/supplier/{supplier_id}',
            path: {
                'supplier_id': supplierId,
            },
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

    /**
     * Get Consensus
     * Get **consensus** about a **request_id**
     *
     * Users or their scheduling agent emit a request for a CDM consensus.
     * When the consensus is ready on the network it is broadcasted for other
     * automated applications (other smart contracts) to process.
     *
     * A **request_id** is the on-chain ID of the initial request made by
     * users/requesters.
     * @param requestId
     * @returns any Successful Response
     * @throws ApiError
     */
    public static getConsensusAppStmV1CdmConsensusRequestIdGet(
        requestId: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/cdm/consensus/{request_id}',
            path: {
                'request_id': requestId,
            },
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

    /**
     * Create Cdm Consensus
     * @param requestBody
     * @returns CDM Successful Response
     * @throws ApiError
     */
    public static createCdmConsensusAppStmV1CdmConsensusPost(
        requestBody: CDMCreate,
    ): CancelablePromise<CDM> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/app/stm/v1/cdm/consensus',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

    /**
     * Get Cdms
     * @returns CDM Successful Response
     * @throws ApiError
     */
    public static getCdmsAppStmV1CdmGet(): CancelablePromise<Array<CDM>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/cdm',
            errors: {
                404: `Lost in STM 404 space`,
            },
        });
    }

    /**
     * Get Cdm Id
     * @param cdmId
     * @returns CDM Successful Response
     * @throws ApiError
     */
    public static getCdmIdAppStmV1CdmCdmIdGet(
        cdmId: string,
    ): CancelablePromise<CDM> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/cdm/{cdm_id}',
            path: {
                'cdm_id': cdmId,
            },
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

    /**
     * Get Cdm Request Id
     * @param requestId
     * @returns CDM Successful Response
     * @throws ApiError
     */
    public static getCdmRequestIdAppStmV1CdmRequestRequestIdGet(
        requestId: string,
    ): CancelablePromise<CDM> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/app/stm/v1/cdm/request/{request_id}',
            path: {
                'request_id': requestId,
            },
            errors: {
                404: `Lost in STM 404 space`,
                422: `Validation Error`,
            },
        });
    }

}
