/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AccountCreate = {
    appear_date?: string;
    last_updated?: string;
    address: string;
};

