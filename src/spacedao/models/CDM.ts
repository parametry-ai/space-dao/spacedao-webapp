/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CDM = {
    cdm: any;
    cdm_id: string;
    request_id: string;
    created?: string;
    rso_1?: string;
    rso_2?: string;
};

