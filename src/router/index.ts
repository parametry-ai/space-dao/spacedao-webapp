import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/orbit-situation'
  },
  {
    path: '/operator',
    component: () => import('../views/OperatorView.vue')
  },
  {
    path: '/ssa-provider',
    component: () => import('../views/SSASupplierView.vue')
  },
  {
    path: '/login',
    component: () => import('../views/LoginView.vue')
  },
  {
    path: '/register-asset',
    component: () => import('../views/RegisterAssetView.vue')
  },
  {
    path: '/register-user',
    component: () => import('../views/RegisterUserView.vue')
  },
  {
    path: '/space-overview',
    component: () => import('../views/SpaceOverviewView.vue'),
  },
  {
    path: '/test',
    component: () => import('../views/TestView.vue')
  },
  {
    path: '/testscroll',
    component: () => import('../views/TestScrollView.vue')
  },
  {
    path: '/user-assets',
    component: () => import('../views/UserAssetsView.vue'),
  },
  {
    path: '/user-assets/:id',
    component: () => import('../views/AssetDetailsView.vue')
  },
  {
    path: '/user-assets/register',
    component: () => import('../views/RegisterAssetView.vue')
  },
  {
    path: '/orbit-situation',
    component: () => import('../views/OrbitView.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
